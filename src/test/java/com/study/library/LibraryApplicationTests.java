package com.study.library;

import com.study.library.domain.Borrow;
import com.study.library.domain.Copy;
import com.study.library.domain.Reader;
import com.study.library.domain.Title;
import com.study.library.service.DbService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest
public class LibraryApplicationTests {

//    @Test
//    public void contextLoads() {
//    }
    @Autowired
    DbService dbService;

//    @Test
//    public void test() {
//        Reader reader = new Reader("Henry", "Johns");
//
//        readerRepository.save(reader);
//    }

    @Test
    public void test2() {
        Title title = new Title("Doomowka", "Janko Wal",2003);
        dbService.newTitle(title);

        Copy copy = new Copy(title,"InUse");
        dbService.newCopy(copy);

        Reader reader = new Reader("Elizabeth", "Sykulak");
        dbService.newReader(reader);

        Borrow borrow = new Borrow(copy, reader);
        dbService.borrowBook(borrow);






    }

}

