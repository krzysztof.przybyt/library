package com.study.library.repository;

import com.study.library.domain.Title;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface TitleRepository extends CrudRepository<Title,Integer> {
    Title findByTitleAndAuthorAndPublicationYear(String title, String author, Integer publicationYear);
}
