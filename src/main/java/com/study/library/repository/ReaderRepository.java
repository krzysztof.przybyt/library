package com.study.library.repository;

import com.study.library.domain.Reader;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ReaderRepository extends CrudRepository<Reader, Integer> {
    @Override
    List<Reader> findAll();

    @Override
    Reader save(Reader reader);

    Reader findByNameAndLastname(String name, String lastname);


}
