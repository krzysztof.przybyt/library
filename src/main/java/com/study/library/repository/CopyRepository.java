package com.study.library.repository;

import com.study.library.domain.Copy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface CopyRepository extends CrudRepository<Copy, Integer> {
    List<Copy> findAll();

    @Override
    Copy save(Copy copy);



}
