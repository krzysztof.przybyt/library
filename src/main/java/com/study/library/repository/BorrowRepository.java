package com.study.library.repository;

import com.study.library.domain.Borrow;
import com.study.library.domain.Copy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Transactional
@Repository
public interface BorrowRepository extends CrudRepository<Borrow, Integer> {
    @Override
    Borrow save(Borrow borrow);

    List<Borrow> findByCopyIdAndReturned(Copy copy, Date date);
}
