package com.study.library.controller;

import com.study.library.domain.*;
import com.study.library.mapper.BorrowMapper;
import com.study.library.mapper.CopyMapper;
import com.study.library.mapper.ReaderMapper;
import com.study.library.mapper.TitleMapper;
import com.study.library.service.DbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/library")
public class LibraryController {

    @Autowired
    private DbService dbService;

    @Autowired
    private ReaderMapper readerMapper;
    @Autowired
    private TitleMapper titleMapper;
    @Autowired
    private CopyMapper copyMapper;
    @Autowired
    private BorrowMapper borrowMapper;

    @RequestMapping(method = RequestMethod.GET, value = "getReaders")
    public List<ReaderDto> getReaders() {
        return readerMapper.mapToReaderDtoList(dbService.getReaders());
    }

    @RequestMapping(method = RequestMethod.GET, value = "getCopies")
    public List<CopyDto> getCopies() {
       return copyMapper.mapToCopyDtoList(dbService.getCopies());
    }

    @RequestMapping(method = RequestMethod.GET, value = "getReaderById/{id}")
    public ReaderDto getReaderById(@PathVariable int id) throws NotFoundException {
        return readerMapper.mapToReaderDto(dbService.findReaderById(id).orElseThrow(NotFoundException::new));
    }

    @RequestMapping(method = RequestMethod.POST, value = "newReader",consumes = APPLICATION_JSON_VALUE)
    public void addReader(@RequestBody ReaderDto readerDto) {
        dbService.newReader(readerMapper.mapToReader(readerDto));
    }

    @RequestMapping(method = RequestMethod.POST, value = "newTitle",consumes = APPLICATION_JSON_VALUE)
    public void addTitle(@RequestBody TitleDto titleDto) {
        dbService.newTitle(titleMapper.mapToTitle(titleDto));
    }

    @RequestMapping(method = RequestMethod.POST, value = "newCopy",consumes = APPLICATION_JSON_VALUE)
    public void addCopy(@RequestBody CopyDto copyDto) {
        dbService.newCopy(copyMapper.mapToCopy(copyDto));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "changeCopyStatus",consumes = APPLICATION_JSON_VALUE)
    public CopyDto changeCopyStatus(@RequestBody CopyDto copyDto) {
        return copyMapper.mapToCopyDto(dbService.newCopy(copyMapper.mapToCopyPut(copyDto)));
    }

    @RequestMapping(method = RequestMethod.GET, value = "countCopiesInUse",consumes = APPLICATION_JSON_VALUE)
    public Long checkCopiesByTitle(@RequestBody TitleDto titleDto) {
        return dbService.countCopiesByTitle(titleMapper.mapToTitle(titleDto));
    }

    @RequestMapping(method = RequestMethod.POST, value = "borrowBook",consumes = APPLICATION_JSON_VALUE)
    public void borrowBook(@RequestBody BorrowDto borrowDto) {
        dbService.borrowBook(borrowMapper.mapToBorrow(borrowDto));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "returnBook",consumes = APPLICATION_JSON_VALUE)
    public BorrowDto returnBook(@RequestBody BorrowDto borrowDto) {
        return borrowMapper.mapToBorrowDto(dbService.returnBook(borrowMapper.mapToBorrow(borrowDto)));
    }
}
