package com.study.library.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "BORROWS")
public class Borrow {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int borrowId;

    @ManyToOne
    @JoinColumn(name = "COPY_ID")
    private Copy copyId;

    @ManyToOne
    @JoinColumn(name = "READER_ID")
    private Reader readerId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "BORROW")
    private Date borrow;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name= "RETURNED")
    private Date returned;

    public Borrow(Copy copy,  Reader reader) {
        this.copyId = copy;
        this.readerId = reader;
        this.borrow = new Date();

    }
}
