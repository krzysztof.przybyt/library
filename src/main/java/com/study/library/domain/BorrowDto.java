package com.study.library.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class BorrowDto {
    private CopyDto copyId;
    private ReaderDto readerId;
    private Date borrow;
    private Date returned;


}
