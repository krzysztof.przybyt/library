package com.study.library.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class TitleDto {
    private Integer titleId;
    private String title;
    private String author;
    private Integer publicationYear;
    private List<CopyDto> copiesDto = new ArrayList<>();



    public TitleDto(Integer titleId, String title, String author, Integer publicationYear) {

        this.titleId = titleId;
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
    }
}
