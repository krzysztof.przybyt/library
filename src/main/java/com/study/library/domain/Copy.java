package com.study.library.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "COPIES")
public class Copy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int copyId;

    @ManyToOne
    @JoinColumn(name = "TITLE_ID")
    private Title title;

    @Column(name = "STATUS")
    private String status;



    @OneToMany(
            targetEntity = Borrow.class,
            mappedBy = "copyId",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<Borrow> borrows = new ArrayList<>();

    public Copy(int id, Title title, String status) {
        this.copyId = id;
        this.title = title;
        this.status = status;
    }

    public Copy(Title title, String status) {
        this.title = title;
        this.status = status;
    }
}
