package com.study.library.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
@Table(
        name = "READERS",
        uniqueConstraints=@UniqueConstraint(columnNames={"NAME", "LASTNAME"})
)
public class Reader {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int readerId;
    @Column(name = "NAME")
    private String name;
    @Column(name = "LASTNAME")
    private String lastname;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date created;
    @OneToMany(
            targetEntity = Borrow.class,
            mappedBy = "readerId",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<Borrow> borrows = new ArrayList<>();

    public Reader(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
        this.created = new Date();
    }
    public Reader(int id, String name, String lastname) {
        this.readerId = id;
        this.name = name;
        this.lastname = lastname;
        this.created = new Date();
    }
}
