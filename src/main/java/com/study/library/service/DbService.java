package com.study.library.service;

import com.study.library.domain.Borrow;
import com.study.library.domain.Copy;
import com.study.library.domain.Reader;
import com.study.library.domain.Title;
import com.study.library.repository.BorrowRepository;
import com.study.library.repository.CopyRepository;
import com.study.library.repository.ReaderRepository;
import com.study.library.repository.TitleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class DbService {
    @Autowired
    private BorrowRepository borrowRepository;
    @Autowired
    private CopyRepository copyRepository;
    @Autowired
    private ReaderRepository readerRepository;
    @Autowired
    private TitleRepository titleRepository;

    public List<Reader> getReaders() {
        return readerRepository.findAll();
    }

    public List<Copy> getCopies() {
        return copyRepository.findAll();
    }

    public Optional<Reader> findReaderById(final int id) {
        return readerRepository.findById(id);
    }

    public void newTitle(final Title title) {
        if(titleRepository.findByTitleAndAuthorAndPublicationYear(
                title.getTitle(),title.getAuthor(),title.getPublicationYear())==null) {
            titleRepository.save(title);
        } else {
            System.out.println("Record is present");
        }
    }

    public Reader newReader(final Reader reader) {
        if(readerRepository.findByNameAndLastname(reader.getName(), reader.getLastname())==null) {

        return readerRepository.save(reader);
        } else {
            System.out.println("Record is present");
            return readerRepository.findByNameAndLastname(reader.getName(),reader.getLastname());
        }
    }

    public Copy newCopy(final Copy copy) {
        newTitle(copy.getTitle());
        Title title = titleRepository.findByTitleAndAuthorAndPublicationYear(
                copy.getTitle().getTitle(),
                copy.getTitle().getAuthor(),
                copy.getTitle().getPublicationYear());
        if(title==null) {
            return copyRepository.save(copy);
        } else {
           copy.setTitle(title);
            return copyRepository.save(copy);
        }
    }

    public Long countCopiesByTitle(final Title title)  {

        Title title2 = titleRepository.findByTitleAndAuthorAndPublicationYear(
                title.getTitle(),
                title.getAuthor(),
                title.getPublicationYear());

        if(title2==null) {
            return 0L;
        } else {
            return title2.getCopies().stream()
                    .map(c -> c.getStatus())
                    .filter(d-> d.contains("inUse"))
                    .count();
        }
    }

    public void borrowBook(final Borrow borrow) {
        List<Borrow> exist = borrowRepository.findByCopyIdAndReturned(borrow.getCopyId(), null);
        if(!exist.isEmpty()) {
            System.out.println("This copy is Borrowed");
        } else {
           borrowRepository.save(borrow);
        }
    }

    public Borrow returnBook(final Borrow borrow) {
        List<Borrow> exist = borrowRepository.findByCopyIdAndReturned(borrow.getCopyId(), null);

        exist.get(0).setReturned(new Date());
        return borrowRepository.save(exist.get(0));

    }

}
