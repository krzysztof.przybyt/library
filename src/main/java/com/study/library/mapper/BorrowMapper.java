package com.study.library.mapper;

import com.study.library.domain.Borrow;
import com.study.library.domain.BorrowDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BorrowMapper {
    @Autowired
    CopyMapper copyMapper;

    @Autowired
    ReaderMapper readerMapper;

    @Autowired
    TitleMapper titleMapper;

    public Borrow mapToBorrow(BorrowDto borrowDto) {
        return new Borrow(copyMapper.mapToCopyPut(borrowDto.getCopyId()),
        readerMapper.mapToReaderPut(borrowDto.getReaderId()));
    }
    public BorrowDto mapToBorrowDto(Borrow borrow) {
        return new BorrowDto(copyMapper.mapToCopyDto(borrow.getCopyId()),
                readerMapper.mapToReaderDto(borrow.getReaderId()),
                borrow.getBorrow(),
                borrow.getReturned());
    }
}
