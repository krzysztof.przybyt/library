package com.study.library.mapper;

import com.study.library.domain.Reader;
import com.study.library.domain.ReaderDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReaderMapper {
    public Reader mapToReader(ReaderDto readerDto) {
        return new Reader(
                readerDto.getName(),
                readerDto.getLastname()
        );
    }
        public Reader mapToReaderPut(ReaderDto readerDto) {
        return new Reader(
                readerDto.getId(),
                readerDto.getName(),
                readerDto.getLastname()
                );
    }
    public ReaderDto mapToReaderDto(Reader reader) {
        return new ReaderDto(
                reader.getReaderId(),
                reader.getName(),
                reader.getLastname(),
                reader.getCreated()
        );
    }
    public List<ReaderDto> mapToReaderDtoList(final List<Reader> readers) {
        return readers.stream()
                .map(r->new ReaderDto(r.getReaderId(), r.getName(), r.getLastname(), r.getCreated()))
                .collect(Collectors.toList());
    }
}
