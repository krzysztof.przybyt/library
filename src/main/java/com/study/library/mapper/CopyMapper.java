package com.study.library.mapper;

import com.study.library.domain.Copy;
import com.study.library.domain.CopyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CopyMapper {

    @Autowired
    TitleMapper titleMapper;

    public Copy mapToCopy(final CopyDto copyDto) {
        return new Copy(titleMapper.mapToTitle(copyDto.getTitle_id()),copyDto.getStatus());
    }
    public Copy mapToCopyPut(final CopyDto copyDto) {
        return new Copy(copyDto.getId(), titleMapper.mapToTitle(copyDto.getTitle_id()),copyDto.getStatus());
    }
    public CopyDto mapToCopyDto(final Copy copy) {
        return new CopyDto(copy.getCopyId(),titleMapper.mapToTitleDto(copy.getTitle()),copy.getStatus());
    }
    public List<CopyDto> mapToCopyDtoList(final List<Copy> list) {
        return list.stream()
                .map(c -> new CopyDto(c.getCopyId(), titleMapper.mapToTitleDto(c.getTitle()), c.getStatus()))
                .collect(Collectors.toList());
    }
}
